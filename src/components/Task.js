import { styled } from "styled-components";
import { FaTimes } from "react-icons/fa";
import { useContext } from "react";
import { TaskContext } from "./TaskContext";

export const Task = ({ task }) => {
  const { removeTask } = useContext(TaskContext);
  const deleteTask = () => {
    removeTask(task.id);
  };

  return (
    <TaskItem>
      <Description>
        {task.task}
        <FaTimes
          style={{ color: "red", cursor: "pointer", float: "right" }}
          onClick={deleteTask}
        />
      </Description>
      <TaskDate>{task.date}</TaskDate>
    </TaskItem>
  );
};

const TaskItem = styled.div`
  background: #ccc;
  border-radius: 2px;
  margin-bottom: 5px;
  text-align: left;
  padding: 10px;
`;

const Description = styled.div`
  margin-bottom: 2px;
`;

const TaskDate = styled.div`
  font-size: 12px;
`;
