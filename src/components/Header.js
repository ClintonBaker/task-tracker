import { styled } from "styled-components";

export const Header = ({ showTaskForm, toggleTaskForm }) => {
  return (
    <Container>
      <Heading>Task List Tracker 4000</Heading>
      <Button
        onClick={toggleTaskForm}
        style={{
          border: `2px solid ${showTaskForm ? "red" : "green"}`,
          background: showTaskForm ? "#fcc" : "#cfc",
          color: showTaskForm ? "red" : "green",
        }}
      >
        {showTaskForm ? "Close" : "Add"}
      </Button>
    </Container>
  );
};

const Container = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  margin-bottom: 20px;
`;

const Heading = styled.span`
  font-family: "Proza Libre", sans-serif;
  font-size: 18px;
`;

const Button = styled.button`
  height: 30px;
  padding-left: 6px;
  padding-right: 6px;
  border-radius: 5px;
  font-weight: 700;
`;
