import { useContext } from "react";
import { TaskContext } from "./TaskContext";
import { Task } from "./Task";

export const TaskList = () => {
  const { tasks } = useContext(TaskContext);

  return (
    <div>
      {tasks.length ? (
        tasks.map((task) => <Task key={task.id} task={task} />)
      ) : (
        <div>No Tasks To Show Yet!</div>
      )}
    </div>
  );
};
