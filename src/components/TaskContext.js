import React, { useReducer } from "react";

export const TaskContext = React.createContext();

const taskReducer = (state, action) => {
  switch (action.type) {
    case "ADD_TASK":
      return [...state, action.payload];
    case "REMOVE_TASK":
      return state.filter((task) => task.id !== action.payload.id);
    default:
      return state;
  }
};

export const TaskProvider = ({ children }) => {
  const [tasks, dispatch] = useReducer(taskReducer, []);

  const addTask = (task) => {
    dispatch({ type: "ADD_TASK", payload: task });
  };

  const removeTask = (taskId) => {
    dispatch({ type: "REMOVE_TASK", payload: { id: taskId } });
  };

  return (
    <TaskContext.Provider value={{ tasks, addTask, removeTask }}>
      {children}
    </TaskContext.Provider>
  );
};
