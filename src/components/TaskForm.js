import { useContext, useState } from "react";
import { TaskContext } from "./TaskContext";
import { styled } from "styled-components";

export const TaskForm = ({ close }) => {
  const [formData, setFormData] = useState({
    task: "",
    date: "",
    reminder: false,
  });

  const { addTask } = useContext(TaskContext);

  const onChangeHandler = (e) => {
    const { name, value } = e.target;
    setFormData({
      ...formData,
      [name]: value,
    });
  };

  const handleSubmit = (e) => {
    e.preventDefault();

    addTask({
      id: new Date().getTime(),
      ...formData,
    });
    close();
  };

  return (
    <Form onSubmit={handleSubmit}>
      <FormItem>
        <label>Task</label>
        <FormInput
          type="text"
          name="task"
          placeholder="Add Task"
          value={formData.task}
          onChange={onChangeHandler}
        />
      </FormItem>
      <FormItem>
        <label>Day & Time</label>
        <FormInput
          type="text"
          name="date"
          placeholder="Add Day & Time"
          value={formData.date}
          onChange={onChangeHandler}
        />
      </FormItem>
      <Button type="submit">Save Task</Button>
    </Form>
  );
};

const Form = styled.form`
  text-align: left;
`;

const FormItem = styled.div`
  margin-bottom: 15px;
`;

const FormInput = styled.input`
  display: block;
  width: 95%;
  margin-top: 6px;
  height: 30px;
  padding-left: 8px;
  border: 2px #bbb solid;
  border-radius: 4px;
  background: #ccc;
`;

const Button = styled.button`
  margin-top: 15px;
  width: 100%;
  height: 32px;
  background: black;
  border-radius: 5px;
  border: none;
  color: white;
  font-weight: bold;
`;
