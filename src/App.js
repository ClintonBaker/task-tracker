import { styled } from "styled-components";
import { Header } from "./components/Header";
import { useState } from "react";

import { TaskProvider } from "./components/TaskContext";
import { TaskForm } from "./components/TaskForm";
import { TaskList } from "./components/TaskList";

export const App = () => {
  const [showTaskForm, setShowTaskForm] = useState(false);

  return (
    <TaskProvider>
      <Container>
        <Tracker>
          <Header
            showTaskForm={showTaskForm}
            toggleTaskForm={() => setShowTaskForm(!showTaskForm)}
          />
          {showTaskForm ? (
            <TaskForm
              close={() => {
                setShowTaskForm(false);
              }}
            />
          ) : (
            <TaskList />
          )}
        </Tracker>
      </Container>
    </TaskProvider>
  );
};

const Container = styled.div`
  text-align: center;
  display: flex;
  justify-content: center;
  background-color: #ddd;
  height: 100vh;
`;

const Tracker = styled.div`
  margin-top: 50px;
  padding: 25px;
  width: 350px;
  height: 300px;
  border: 1px solid black;
  border-radius: 15px;
`;
